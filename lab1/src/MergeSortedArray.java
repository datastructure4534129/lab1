import java.util.Scanner;

public class MergeSortedArray {
    public static int[] nums1;
    public static int[] nums2;
    static int m;
    static int n;

    public static void main(String[] args) {
        inputM();
        inputN();
        inputNums1();
        inputNums2();
        MergeAndSort();
    }

    private static void inputNums2() {
        Scanner kb = new Scanner(System.in);
        nums2 = new int[n];
        System.out.print("Input elements of nums2: ");
        for (int i = 0; i < n; i++) {
            nums2[i] = kb.nextInt();
        }
    }

    private static void inputNums1() {
        Scanner kb = new Scanner(System.in);
        nums1 = new int[m + n];
        System.out.print("Input elements of nums1: ");
        for (int i = 0; i < m; i++) {
            nums1[i] = kb.nextInt();
        }

        for (int i = m; i < m + n; i++) {
            nums1[i] = 0;
        }
    }

    private static void MergeAndSort() {
        int i = m - 1;
        int j = n - 1;
        int k = m + n - 1;

        while (i >= 0 && j >= 0) {
            if (nums1[i] >= nums2[j]) {
                nums1[k] = nums1[i];
                i--;
            } else {
                nums1[k] = nums2[j];
                j--;
            }
            k--;
        }

        while (j >= 0) {
            nums1[k] = nums2[j];
            j--;
            k--;
        }

        System.out.print("[ ");
        for (int num : nums1) {
            System.out.print(num + " ");
        }
        System.out.print("]");
    }

    private static void inputN() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Input N: ");
        n = kb.nextInt();
    }

    private static void inputM() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Input M: ");
        m = kb.nextInt();
    }
}



