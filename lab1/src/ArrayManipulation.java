import java.util.Arrays;

public class ArrayManipulation {
    static int numbers[] = { 5, 8, 3, 2, 7 };
    static String names[] = { "Alice", "Bob", "Charlie", "David" };
    static double values[] = new double[4];

    public static void main(String[] args) {
        printNumbers();
        printNames();
        initialAndcalculateValues();
        findAndprintMaximumValues();
        reversedNames();
        sortNumbers();

    }

    public static void printNumbers() {
        for (int i = 0; i < 5; i++) {
            System.out.print(numbers[i] + " ");
        }
        System.out.println();
    }

    public static void printNames() {
        for (String i : names) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public static void initialAndcalculateValues() {
        values[0] = 10.1;
        values[1] = 10.2;
        values[2] = 10.3;
        values[3] = 10.4;
        double sum = 0.00;
        for (int i = 0; i < 4; i++) {
            sum += values[i];
        }
        System.out.println(sum);
    }

    public static void findAndprintMaximumValues() {
        double maximum = values[0];
        for (int i = 1; i < 4; i++) {
            if (values[i] > maximum) {
                maximum = values[i];
            }
        }
        System.out.println(maximum);
    }

    public static void reversedNames() {
        int namesSize = names.length;
        String reversedNames[] = new String[namesSize];
        for (int i = 1; i <= namesSize; i++) {
            reversedNames[i - 1] = names[namesSize - i];
        }
        System.out.println(Arrays.toString(reversedNames));
    }

    public static void sortNumbers() {
        Arrays.sort(numbers);
        System.out.println(Arrays.toString(numbers));
    }

}

